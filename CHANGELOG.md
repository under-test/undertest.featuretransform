# Changelog
All notable changes to this project will be documented in this file.

## [unreleased]
### added
- new annotate feature that generates markdown version of feature files with annotations #56
- improve general failure output #65
- add support for `Rule` to Excel #66
- add support for empty features to Excel #67
### changed

## [0.5.0] - 2019-10-30
### added
- add markdown report support #43

## [0.4.0] - 2019-08-21
### added
- add Scenario list to feature Summary #40
### changed
- removed invalid characters from worksheet names #39

## [0.3.0] - 2019-08-05
### added
- summary results on Table of Feature page now summarize results per feature
### changed
- created Excel and summary classes
- moved excel and summary create methods from program.cs to respective classes
- result cells are now only available for steps and example rows

## [0.2.1] - 2019-05-07
### added 
### changed
- corrected an issue with features in child folders #27
- corrected issue where executing non-working directory fails #28

## [0.3.0-alpha] - 2019-04-27
### added
- table of contents to feature summary with links to each directory header

### changed
- now prints name of root directory name instead of `\`

## [0.2.0] - 2019-04-15
### added 
- summary option added to create summary page of feature files in a project
- adds a failure ExitCode on failure
### changed
- adds invalid gherkin file handling to `Excel`

## [0.1.1] - 2019-04-09
### changed
- fixed issue where when passed a folder the glob was incorrect - #11

## [0.1.0] - 2019-02-17
- initial version
