namespace UnderTest.FeatureTransform.UnitTests.Stubs
{
  using System.Collections.Generic;

  using UnderTest.FeatureTransform.Options;

  class StubStandardVerbOptions : IStandardVerbOptions
  {
    public IEnumerable<string> FeaturePaths { get; set; }

    public string OutputFilename { get; set; }

    public string WorkingDirectory { get; set; }
  }
}
