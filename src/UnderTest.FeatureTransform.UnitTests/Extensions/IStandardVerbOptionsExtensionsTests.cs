namespace UnderTest.FeatureTransform.UnitTests.Extensions
{
  using FluentAssertions;

  using NUnit.Framework;

  using UnderTest.FeatureTransform.Options;
  using UnderTest.FeatureTransform.UnitTests.Stubs;

  public class IStandardVerbOptionsExtensionsTests
  {
    [Test]
    public void DefaultWorkingDirectory_WhenEmpty_UsesCurrentDirectory()
    {
      const string WorkingDirectory = @"c:\temp";
      var instance = new StubStandardVerbOptions();

      instance.SetDefaultWorkingDirectoryIfNonWhiteSpace(WorkingDirectory);

      instance.WorkingDirectory.Should().Be(WorkingDirectory);
    }

    [Test]
    public void DefaultWorkingDirectory_WhenNonEmpty_LeavesFolderUntouched()
    {
      const string CurrentDirectory = @"c:\windows";
      const string WorkingDirectory = @"c:\temp";
      var instance = new StubStandardVerbOptions { WorkingDirectory = CurrentDirectory };

      instance.SetDefaultWorkingDirectoryIfNonWhiteSpace(WorkingDirectory);

      instance.WorkingDirectory.Should().Be(CurrentDirectory);
    }
  }
}
