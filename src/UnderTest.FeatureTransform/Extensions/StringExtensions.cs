﻿using System.IO;

namespace System
{
  public static class StringExtensions
  {
    public static void CreateDirectory(this string pathP)
    {
      if (!Directory.Exists(pathP))
      {
        Directory.CreateDirectory(pathP);
      }
    }

    public static string IndentStep(this string stepP)
    {
      return stepP == "And" ? stepP.Indent().Indent() : stepP.Indent();
    }

    public static string Indent(this string textP)
    {
      return "&nbsp;&nbsp;"+textP;
    }

    public static string ToAnchorLink(this string textP)
    {
      return "[" + textP + "]" + "(#" + textP.Replace(" ", "-").ToLower() + ")";
    }

    public static string Bullet(this string textP)
    {
      return "* " + textP;
    }

    public static string MakeParamsViewableInMarkdown(this string textP)
    {
      return textP.Replace("<", "**\\<").Replace(">", ">**");
    }

    public static string Bold(this string textP)
    {
      return " **"+textP + "** ";
    }

    public static string Quote(this string textP)
    {
      return ">" + textP;
    }

    public static string HeaderThree(this string textP)
    {
      return "### " + textP;
    }

    public static string HeaderTwo(this string textP)
    {
      return "## " + textP;
    }

    public static string NewLineEndingSpace(this string textP)
    {
      return textP + "  ";
    }

    public static string CodeBlock(this string textP)
    {
      return "```\n"+textP+"\n```";
    }
  }
}
