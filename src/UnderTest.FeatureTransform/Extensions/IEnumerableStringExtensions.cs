﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Gherkin.Ast;

namespace System.Collections
{
  public static class IEnumerableStringExtensions
  {
    public static IEnumerable<GherkinDocument> ConvertToGherkinDocuments(this IEnumerable<string> filePathsP, string directoryP)
    {
      var gherkinDocuments = new List<GherkinDocument>();
      foreach (var file in filePathsP)
      {
        gherkinDocuments.Add( new Gherkin.Parser().Parse(Path.Combine(directoryP, file)));
      }

      return gherkinDocuments;
    }
  }
}
