using System.Collections.Generic;

namespace UnderTest.FeatureTransform.WorkBooks
{
  public static class StringExtensions
  {
    public static string Truncate(this string value, int maxChars)
    {
      return value.Length <= maxChars ? value : value.Substring(0, maxChars);
    }

    public static string RemoveBadCharsForWorksheetName(this string value)
    {
      var badCharList = new Dictionary<string, string>
      {
        {"'", ""},
        {@"\", "_"},
        {@"/", "_"}
      };
      foreach (var (key, s) in badCharList)
      {
        value = value.Replace(key, s);
      }
      return value;
    }
  }
}
