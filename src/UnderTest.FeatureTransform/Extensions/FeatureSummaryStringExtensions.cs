using System.Text.RegularExpressions;

namespace UnderTest.FeatureTransform.Summary
{
  public static class FeatureSummaryStringExtensions
  {
    public static string MakeHeaderLink(this string value)
    {
      value = value.ToLower();
      value = value.Replace(" ", "-");
      value = value.Replace("\\", "");
      value = value.Replace("/", "");
      value = Regex.Replace(value, "[^a-zA-Z0-9 -]", "-");
      if (value == string.Empty) return value;
      while (value.Contains("--"))
      {
        value = value.Replace("--", "-");
      }

      value = value.Trim('-');

      return value;
    }

    public static string UseBackslashForDirectories(this string value)
    {
      return value.Replace("%5C","\\");
    }
  }
}
