﻿using System.Collections.Generic;
using Gherkin.Ast;

namespace UnderTest.FeatureTransform.AnnotateFeature.Models
{
  public class AnnotatedFeatureDetails
  {
    public Feature Feature { get; set; }
    public Dictionary<string, string> Definitions { get; set; }
  }
}
