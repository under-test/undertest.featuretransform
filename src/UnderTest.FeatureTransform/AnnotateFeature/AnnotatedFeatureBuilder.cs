﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Gherkin.Ast;
using UnderTest.FeatureTransform.AnnotateFeature.Models;
using UnderTest.FeatureTransform.Summary;
using Examples = Gherkin.Ast.Examples;
using Feature = Gherkin.Ast.Feature;
using Scenario = Gherkin.Ast.Scenario;
using Step = Gherkin.Ast.Step;
using StepsContainer = Gherkin.Ast.StepsContainer;
using TableRow = Gherkin.Ast.TableRow;

namespace UnderTest.FeatureTransform.AnnotateFeature
{
  public class AnnotatedFeatureBuilder
  {
    public AnnotatedFeatureBuilder(AnnotatedFeatureDetails detailsP)
    {
      feature = detailsP.Feature;
      definitions = detailsP.Definitions;
      annotatedFeature = new StringBuilder();
      footNotes = new Dictionary<string, string>();
    }

    private Feature feature;
    private StringBuilder annotatedFeature;
    private Dictionary<string, string> definitions;

    private Dictionary<string, string> footNotes;

    public AnnotatedFeatureBuilder AddFeatureScenarios()
    {
      annotatedFeature.AppendLine("Scenarios".HeaderTwo());
      foreach (var child in feature.Children)
      {
        if (child is Scenario scenario)
        {
          AddScenario(scenario);
        }
      }

      return this;
    }

    public AnnotatedFeatureBuilder AddFootNotes()
    {
      foreach (var (key, value) in footNotes)
      {
        annotatedFeature.AppendLine($"[^{key}]: {value}");
      }

      return this;
    }

    public AnnotatedFeatureBuilder AddListOfScenarios()
    {
      annotatedFeature.AppendLine("Scenario List".HeaderTwo());
      foreach (var child in feature.Children)
      {
        if (child is Scenario scenario)
        {
          annotatedFeature.AppendLine($"* [{scenario.Name}](#"+(scenario.Keyword+" "+scenario.Name).MakeHeaderLink()+")");
        }
      }
      return this;
    }

    private void AddScenario(Scenario scenarioP)
    {
      switch (scenarioP.Keyword)
      {
        case "Scenario":
          AddScenarioWithNoExampleTable(scenarioP);
          break;
        case "Scenario Outline":
          AddScenarioOutline(scenarioP);
          break;
      }
    }

    private void AddScenarioOutline(Scenario scenarioP)
    {
      annotatedFeature.AppendLine($"{scenarioP.Keyword}: {scenarioP.Name}".HeaderThree());
      annotatedFeature.AppendLine(scenarioP.Description.CodeBlock());
      AddSteps(scenarioP.Steps);
      AddExamples(scenarioP.Examples);
      annotatedFeature.AppendLine();
      annotatedFeature.AppendLine("---");
    }

    private void AddScenarioWithNoExampleTable(StepsContainer scenarioP)
    {
      annotatedFeature.AppendLine($"{scenarioP.Keyword}: {scenarioP.Name}".HeaderThree());
      annotatedFeature.AppendLine(scenarioP.Description);
      AddSteps(scenarioP.Steps);
      annotatedFeature.AppendLine();
      annotatedFeature.AppendLine("---");
    }

    private void AddExamples(IEnumerable<Examples> examplesP)
    {
      foreach (var example in examplesP)
      {
        AddExampleTable(example);
      }
    }

    private void AddExampleTable(Examples examplesP)
    {
      annotatedFeature.AppendLine($"{examplesP.Keyword}:".Bold()+examplesP.Name.NewLineEndingSpace());
      annotatedFeature.AppendLine(examplesP.Description);
      annotatedFeature.AppendLine("<table>");
      AddHeaderRow(examplesP.TableHeader);
      AddExampleRows(examplesP.TableBody);
      annotatedFeature.AppendLine("</table>");
      annotatedFeature.AppendLine();
    }

    private void AddExampleRows(IEnumerable<TableRow> tableBodyP)
    {
      foreach (var row in tableBodyP)
      {
        AddValuesRow(row);
      }
    }

    private void AddSteps(IEnumerable<Step> stepsP)
    {
      foreach (var step in stepsP)
      {
        annotatedFeature.AppendLine(AddAnnotations(step.Keyword.Trim().IndentStep().Bold()+step.Text.MakeParamsViewableInMarkdown()+"  "));
        if(step.Argument != null)AddStepArgument(step.Argument);
      }
    }

    private string AddAnnotations(string stepP)
    {
      foreach (var (key, value) in definitions.Where(definition => stepP.ToLower().Contains(definition.Key.ToLower())))
      {
        var footnoteKey = key.Replace(" ", string.Empty);
        stepP = stepP.Replace(key, $"{key}[^{footnoteKey}]");
        if (!footNotes.ContainsKey(footnoteKey))
        {
          footNotes.Add(footnoteKey, value);
        }
      }

      return stepP;
    }

    private void AddStepArgument(StepArgument argumentP)
    {
      if (argumentP.GetType() == typeof(DataTable))
      {
        var table = (DataTable) argumentP;
        AddTable(table);
      }

      if (argumentP.GetType() == typeof(DocString))
      {
        var docString = (DocString) argumentP;
        AddDocString(docString);
      }
    }

    private void AddDocString(DocString docStringP)
    {
      annotatedFeature.AppendLine($"{docStringP.Content}".Quote());
      annotatedFeature.AppendLine();
    }

    private void AddTable(IHasRows tableP)
    {
      annotatedFeature.Append("<table>");
      for (var i = 0; i < tableP.Rows.Count(); i++)
      {
        if (i == 0)
        {
          AddHeaderRow(tableP.Rows.ToArray()[i]);
        }

        if (i>0)
        {
          AddValuesRow(tableP.Rows.ToArray()[i]);
        }
      }
      annotatedFeature.AppendLine("</table>");
      annotatedFeature.AppendLine();
    }

    private void AddValuesRow(TableRow rowP)
    {
      annotatedFeature.Append("<tr>");
      foreach (var cell in rowP.Cells)
      {
        annotatedFeature.Append($"<td>"+cell.Value+"</td>");
      }

      annotatedFeature.Append("</tr>");
    }

    private void AddHeaderRow(TableRow rowP)
    {
      annotatedFeature.Append("<tr>");
      foreach (var cell in rowP.Cells)
      {
        annotatedFeature.Append($"<th>" + cell.Value + "</th>");
      }

      annotatedFeature.Append("</tr>");
    }

    public AnnotatedFeatureBuilder AddFeatureName()
    {
      annotatedFeature.AppendLine($"# {feature.Keyword}: {feature.Name}");
      return this;
    }

    public AnnotatedFeatureBuilder AddFeatureTags()
    {
      annotatedFeature.AppendLine("| Feature Tags |");
      annotatedFeature.AppendLine("|---|");
      foreach (var tag in feature.Tags)
      {
        annotatedFeature.AppendLine($"| {tag.Name} |");
      }

      return this;
    }

    public AnnotatedFeatureBuilder AddFeatureDescription()
    {
      annotatedFeature.AppendLine("## Description");
      annotatedFeature.AppendLine("```");
      annotatedFeature.AppendLine($"{feature.Description}");
      annotatedFeature.AppendLine("```");
      return this;
    }

    public string GetAnnotatedFeature()
    {
      return annotatedFeature.ToString();
    }
  }
}
