﻿using System;
using System.Collections.Generic;
using System.IO;
using Gherkin.Ast;
using Newtonsoft.Json;
using UnderTest.FeatureTransform.AnnotateFeature.Models;
using UnderTest.FeatureTransform.Options;

namespace UnderTest.FeatureTransform.AnnotateFeature
{
  public class AnnotateFeature
  {
    public AnnotateFeature(AnnotatedFeatureOptions optionP, IEnumerable<GherkinDocument> gherkinDocumentsP)
    {
      options = optionP;
      gherkinDocuments = gherkinDocumentsP;
    }

    private static AnnotatedFeatureOptions options;
    private IEnumerable<GherkinDocument> gherkinDocuments;

    private Dictionary<string,string> LoadLexicon()
    {
      try
      {
        using (var r = new StreamReader($"{options.WorkingDirectory}\\feature_annotations.json"))
        {
          var json = r.ReadToEnd();
          return JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
        }
      }
      catch (Exception e)
      {
        Console.WriteLine("Failed to read lexicon: "+e);
        throw;
      }
    }

    public void CreateAnnotatedFeatures()
    {
      var annotatedFeaturesDirectory = options.WorkingDirectory + "\\AnnotatedFeatures";
      annotatedFeaturesDirectory.CreateDirectory();
      var definitions = LoadLexicon();

      foreach (var gherkinDocument in gherkinDocuments)
      {
        var annotationsDetails = new AnnotatedFeatureDetails
        {
          Feature = gherkinDocument.Feature,
          Definitions = definitions
        };
        CreateAnnotatedFeature(annotationsDetails);
      }
    }

    private void CreateAnnotatedFeature(AnnotatedFeatureDetails detailsP)
    {
      var annotatedFeature = new AnnotatedFeatureBuilder(detailsP)
        .AddFeatureName()
        .AddFeatureTags()
        .AddFeatureDescription()
        .AddListOfScenarios()
        .AddFeatureScenarios()
        .AddFootNotes()
        .GetAnnotatedFeature();
      var annotatedFeaturePath = options.WorkingDirectory+"\\AnnotatedFeatures\\" + detailsP.Feature.Name + "_Annotated.md";
      File.WriteAllText(annotatedFeaturePath, annotatedFeature);
    }
  }
}
