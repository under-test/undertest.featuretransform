﻿using System.Text;
using UnderTest.FeatureTransform.Markdown.Model;

namespace UnderTest.FeatureTransform.Markdown
{
  public class SummaryResultReportBuilder
  {
    public SummaryResultReportBuilder(UnderTestReport underTestReportP)
    {
      summaryReport = new StringBuilder();
      underTestReport = underTestReportP;
    }

    private readonly StringBuilder summaryReport;
    private readonly UnderTestReport underTestReport;
    public SummaryResultReportBuilder AddSummaryReportScenarioResults()
    {
      summaryReport.AppendLine("### Scenario Results");
      summaryReport.AppendLine("| Details | \r\n |---|");
      summaryReport.AppendLine($"|Passed: {underTestReport.Scenarios.Passed}|");
      summaryReport.AppendLine($"|Failed: {underTestReport.Scenarios.Failed}|");
      summaryReport.AppendLine($"|Inconclusive: {underTestReport.Scenarios.Inconclusive}|");
      summaryReport.AppendLine($"|Skipped: {underTestReport.Scenarios.Skipped}|");
      summaryReport.AppendLine($"|Wip: {underTestReport.Scenarios.Wip}|");
      summaryReport.AppendLine($"|Total: {underTestReport.Scenarios.Total}|");
      return this;
    }

    public SummaryResultReportBuilder AddSummaryReportDuration()
    {
      summaryReport.AppendLine("| Start Time | End Time | Duration |\r\n |---|---|---|");
      summaryReport.AppendLine(
        $"| {underTestReport.ExecutionTime.StartTime.ToString()} | {underTestReport.ExecutionTime.EndTime.ToString()} | {underTestReport.ExecutionTime.Duration.ToString()} |");
      return this;
    }

    public SummaryResultReportBuilder AddSummaryOverallResult()
    {
      summaryReport.AppendLine($"## Overall Result: {underTestReport.OverallResult.Name}");
      return this;
    }

    public SummaryResultReportBuilder AddSummaryReportVersion()
    {
      summaryReport.AppendLine($"## Version: {underTestReport.ProjectVersion}");
      return this;
    }

    public SummaryResultReportBuilder AddSummaryReportTitle()
    {
      summaryReport.AppendLine($"# Project: {underTestReport.ProjectName}");
      return this;
    }

    public SummaryResultReportBuilder AddFeatureTable()
    {
      summaryReport.AppendLine("### Features");
      summaryReport.AppendLine("| FileName | Results |");
      summaryReport.AppendLine("|---|---|");
      AddFeaturesToFeatureTable();
      return this;
    }

    private void AddFeaturesToFeatureTable()
    {
      foreach (var feature in underTestReport.Features)
      {
        summaryReport.AppendLine(
          $"|[{feature.Name}](.\\{feature.FeatureFileName}.md)|");
      }
    }

    public string GetSummaryResultReport()
    {
      return summaryReport.ToString();
    }
  }
}
