namespace UnderTest.FeatureTransform.Markdown.Model
{
  public class TestRunFeatureResultList
  {
    public int Passed { get; set; }

    public int Failed { get; set; }

    public int Inconclusive { get; set; }

    public int Skipped { get; set; }

    public int Wip { get; set; }
  }
}
