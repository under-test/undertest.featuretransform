using System.Collections.Generic;
using Newtonsoft.Json;

namespace UnderTest.FeatureTransform.Markdown.Model
{
  public class ScenarioTestResult
  {
    [JsonProperty("Description")]
    public string Description { get; set; }

    [JsonProperty("Keyword")]
    public string Keyword { get; set; }

    [JsonProperty("ScenarioName")]
    public string ScenarioName { get; set; }

    [JsonProperty("Result")]
    public OverallResult Result { get; set; }

    [JsonProperty("StepResults")]
    public IList<StepResult> StepResults { get; set; }

    [JsonProperty("Errors")]
    public IList<StepNotice> Errors { get; set; }
  }
}
