using Newtonsoft.Json;

namespace UnderTest.FeatureTransform.Markdown.Model
{
  public class Location
  {
    [JsonProperty("Line")]
    public int Line { get; set; }

    [JsonProperty("Column")]
    public int Column { get; set; }
  }
}
