using System.Collections.Generic;
using Newtonsoft.Json;

namespace UnderTest.FeatureTransform.Markdown.Model
{
  public class Feature
  {
    [JsonProperty("Name")]
    public string Name { get; set; }

    [JsonProperty("FeatureDescription")]
    public string FeatureDescription { get; set; }

    [JsonProperty("FeatureFileName")]
    public string FeatureFileName { get; set; }

    [JsonProperty("StrategyRuns")]
    public IList<StrategyRun> StrategyRuns { get; set; }
  }
}
