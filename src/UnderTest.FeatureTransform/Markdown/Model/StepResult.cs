using System;
using System.Collections.Generic;
using Gherkin.Ast;
using Newtonsoft.Json;
using DataTable = DocumentFormat.OpenXml.Drawing.Charts.DataTable;

namespace UnderTest.FeatureTransform.Markdown.Model
{
  public class StepResult
  {
    [JsonProperty("Keyword")]
    public string Keyword { get; set; }

    [JsonProperty("Step")]
    public string Step { get; set; }

    [JsonProperty("Location")]
    public Location Location { get; set; }

    [JsonProperty("HandlerName")]
    public string HandlerName { get; set; }

    [JsonProperty("HandlingMethod")]
    public string HandlingMethod { get; set; }

    [JsonProperty("ScenarioStepResultType")]
    public OverallResult ScenarioStepResultType { get; set; }

    [JsonProperty("Duration")]
    public TimeSpan Duration { get; set; }

    [JsonProperty("LogMessages")]
    public IList<LogMessage> LogMessages { get; set; }

    [JsonProperty("Screenshots")]
    public IList<ScreenShots> Screenshots { get; set; }

    [JsonProperty("Warnings")]
    public IList<StepNotice> Warnings { get; set; }

    [JsonProperty("Errors")]
    public IList<StepNotice> Errors { get; set; }

    [JsonProperty("DataTable")]
    public DataTable DataTable { get; set; }

    [JsonProperty("DocString")]
    public DocString DocString { get; set; }

    [JsonProperty("StepParameters")]
    public IList<string> StepParameters { get; set; }
  }
}
