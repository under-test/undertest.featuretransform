using System.IO;
using System.Text;
using Newtonsoft.Json;
using UnderTest.FeatureTransform.Markdown.Model;
using UnderTest.FeatureTransform.Options;

namespace UnderTest.FeatureTransform.Markdown
{
  public class MarkdownReport
  {
    public MarkdownReport(MarkdownOptions optionsP)
    {
      options = optionsP;
      summaryResultReport = new StringBuilder();
    }

    private readonly MarkdownOptions options;

    private string storeReportPath;
    private UnderTestReport underTestReport;

    private StringBuilder summaryResultReport;

    public void WriteReportFiles()
    {
      storeReportPath = Path.Combine(options.WorkingDirectory, "MarkdownReport");

      CheckDir(storeReportPath);

      underTestReport = LoadJsonReport(options.ReportFile);

      summaryResultReport.Append( new SummaryResultReportBuilder(underTestReport)
        .AddSummaryReportTitle()
        .AddSummaryReportVersion()
        .AddSummaryOverallResult()
        .AddSummaryReportDuration()
        .AddSummaryReportScenarioResults()
        .AddFeatureTable()
        .GetSummaryResultReport());
      File.WriteAllText(Path.Combine(storeReportPath, options.OutputFilename), summaryResultReport.ToString());
      CreateFeatureReports();
    }

    private void CreateFeatureReports()
    {
      foreach (var feature in underTestReport.Features)
      {
        BuildFeatureReport(feature);
      }
    }

    private void BuildFeatureReport(Feature feature)
    {
      var relativePath = Path.GetDirectoryName(feature.FeatureFileName);
      CheckDir(Path.Combine(storeReportPath, relativePath));
      var featureReport = new FeatureReportBuilder(feature)
        .AddFeatureReportName()
        .AddFeatureReportDescription()
        .AddFeatureReportStrategyRuns()
        .GetFeatureReport();
      File.WriteAllText(Path.Combine(storeReportPath, $"{feature.FeatureFileName}.md"), featureReport);
      CheckDir(Path.Combine(storeReportPath, relativePath));
    }

    private UnderTestReport LoadJsonReport(string pathP)
    {
      using (var reader = new StreamReader(pathP))
      {
        var fileText = reader.ReadToEnd();
        return JsonConvert.DeserializeObject<UnderTestReport>(fileText);
      }
    }

    private void CheckDir(string pathP)
    {
      if (!Directory.Exists(pathP))
      {
        Directory.CreateDirectory(pathP);
      }
    }
  }
}
