﻿using System.Linq;
using System.Text;
using UnderTest.FeatureTransform.Markdown.Model;
using static UnderTest.FeatureTransform.Constants.FeatureReportConstants;

namespace UnderTest.FeatureTransform.Markdown
{
  public class FeatureReportBuilder
  {
    public FeatureReportBuilder(Feature featureResultsP)
    {
      featureReport = new StringBuilder();
      featureResults = featureResultsP;
    }

    private StringBuilder featureReport;
    private Feature featureResults;

    public FeatureReportBuilder AddFeatureReportStrategyRuns()
    {
      foreach (var run in featureResults.StrategyRuns)
      {
        AddRunToFeatureReport(run);
      }
      return this;
    }

    private void AddRunToFeatureReport(StrategyRun runP)
    {
      featureReport.AppendLine(RunTableHeader);
      featureReport.AppendLine($"| {runP.StartTime.ToString()} | {runP.EndTime.ToString()} | {runP.Duration.ToString()} |");
      AddTestResults(runP);
    }

    private void AddTestResults(StrategyRun runP)
    {
      foreach (var testResult in runP.ScenarioTestResults)
      {
        featureReport.AppendLine($"### {testResult.Keyword} {testResult.ScenarioName} : {testResult.Result.Name}");

        featureReport.AppendLine($"{testResult.Description}");

        featureReport.AppendLine(TestResultTableHeader);

        AddStepResults(testResult);
      }
    }

    private void AddStepResults(ScenarioTestResult testResultP)
    {
      foreach (var stepResults in testResultP.StepResults)
      {
        featureReport.Append($"|{stepResults.Keyword}|`{stepResults.Step}`|{stepResults.ScenarioStepResultType.Name}|");

        featureReport.Append(stepResults.StepParameters != null ? $"{string.Join("; ", stepResults.StepParameters)}|" : "|");

        featureReport.Append($"{string.Join("; ", stepResults.LogMessages.Select(x => $"```{x.Type} : {x.Message}```"))}|");

        featureReport.Append($"{stepResults.Duration.ToString()}|");
      }
    }

    public FeatureReportBuilder AddFeatureReportDescription()
    {
      featureReport.AppendLine($">{featureResults.FeatureDescription}");
      featureReport.AppendLine(">>>");
      return this;
    }

    public FeatureReportBuilder AddFeatureReportName()
    {
      featureReport.AppendLine($"# {featureResults.Name}");
      featureReport.AppendLine(">>>");
      return this;
    }

    public string GetFeatureReport()
    {
      return featureReport.ToString();
    }
  }
}
