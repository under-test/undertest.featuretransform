using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.IO;
using Gherkin.Ast;
using UnderTest.FeatureTransform.Options;
using UnderTest.FeatureTransform.Sheets;

namespace UnderTest.FeatureTransform.WorkBooks
{
  public class QaWorkBook
  {
    public QaWorkBook(ExcelOptions options, IEnumerable<string> featureFiles)
    {
      _workBook = new XLWorkbook();
      _featureFiles = featureFiles;
      _resultOptionSheet = new ResultOptionSheet(_workBook, ResultOptions);
      _options = options;
      _tofSheet = new TofSheet(_workBook);
    }

    private readonly XLWorkbook _workBook;
    private readonly TofSheet _tofSheet;
    private readonly ResultOptionSheet _resultOptionSheet;
    private readonly ExcelOptions _options;
    private readonly IEnumerable<string> _featureFiles;
    private readonly List<GherkinDocument> validFeatures = new List<GherkinDocument>();
    private readonly List<string> invalidFeatures = new List<string>();
    private static readonly List<string> ResultOptions = new List<string>
    {
      "Pass",
      "Fail",
      "Incomplete"
    };

    public void Process()
    {
      CreateFeatureSheets();
      SetSheetPositions();
      SaveWorkBook();
    }

    private void SaveWorkBook()
    {
      _workBook.SaveAs(Path.Combine(_options.WorkingDirectory, _options.OutputFilename));
    }

    private void SetSheetPositions()
    {
      _tofSheet.SetPosition(1);
      _resultOptionSheet.SetPosition(_workBook.Worksheets.Count + 1);
    }

    private void CreateFeatureSheets()
    {
      LoadFeatures(_featureFiles);
      foreach (var feature in validFeatures)
      {
        var newFeatureSheet = new FeatureSheet(_workBook, feature, ResultOptions)
          .Build();
        _tofSheet.CreateFeatureEntry(feature.Feature.Name, newFeatureSheet.FeatureResultCells);
      }
      foreach (var invalidFeature in invalidFeatures)
      {
        _tofSheet.CreateInvalidFeatureEntry(invalidFeature);
      }
    }

    private void LoadFeatures(IEnumerable<string> featureFilesP)
    {
      if (featureFilesP == null)
      {
        throw new ArgumentNullException(nameof(featureFilesP));
      }

      foreach (var file in featureFilesP)
      {
        CheckForValidFeatureAndAddToList(file);
      }
    }

    private void CheckForValidFeatureAndAddToList(string fileP)
    {
      try
      {
        var potentialFeature = new Gherkin.Parser().Parse(Path.Combine(_options.WorkingDirectory, fileP));
        if (potentialFeature.Feature.Name == string.Empty)
        {
          invalidFeatures.Add(fileP);
          Console.WriteLine($"Invalid feature file ({fileP}) has no feature name. The file will be skipped.");
        }
        else
        {
          validFeatures.Add(new Gherkin.Parser().Parse(Path.Combine(_options.WorkingDirectory, fileP)));
        }
      }
      catch (Exception e)
      {
        invalidFeatures.Add(fileP);
        Console.WriteLine($"Invalid feature file ({fileP}). The file will be skipped. Error: {e.Message}");
      }
    }
  }
}
