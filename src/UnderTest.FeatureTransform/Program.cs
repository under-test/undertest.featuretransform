using System;
using System.Collections;
using System.IO;
using System.Linq;
using CommandLine;
using UnderTest.FeatureTransform.Markdown;
using UnderTest.FeatureTransform.Options;
using UnderTest.FeatureTransform.Summary;
using UnderTest.FeatureTransform.WorkBooks;
using static UnderTest.FeatureTransform.IO.FileCollector;

namespace UnderTest.FeatureTransform
{
  class Program
  {
    static int Main(string[] args)
    {
      // todo: handle duplicate feature names
      var workingDirectory = Directory.GetCurrentDirectory();
      try
      {
        Parser.Default.ParseArguments<ExcelOptions, SummaryOptions, MarkdownOptions, AnnotatedFeatureOptions>(args)
          .WithParsed<ExcelOptions>(o =>
            {
              o.SetDefaultWorkingDirectoryIfNonWhiteSpace(workingDirectory);
              var files = CollectFilesFromGlobs(o.WorkingDirectory, o.FeaturePaths);

              new QaWorkBook(o, files).Process();
            })
          .WithParsed<SummaryOptions>(o =>
            {
              o.SetDefaultWorkingDirectoryIfNonWhiteSpace(workingDirectory);
              var files = CollectFilesFromGlobs(o.WorkingDirectory, o.FeaturePaths);
              new FeatureSummary(o, files).CreateFeatureSummary();
            })
          .WithParsed<MarkdownOptions>(o =>
          {
            o.SetDefaultWorkingDirectoryIfNonWhiteSpace(workingDirectory);
            new MarkdownReport(o).WriteReportFiles();
          })
          .WithParsed<AnnotatedFeatureOptions>(o =>
          {
            o.SetDefaultWorkingDirectoryIfNonWhiteSpace(workingDirectory);

            var files = CollectFilesFromGlobs(o.WorkingDirectory, o.FeaturePaths).ToList();
            var features = files.ConvertToGherkinDocuments(o.WorkingDirectory);
            new AnnotateFeature.AnnotateFeature(o, features).CreateAnnotatedFeatures();
          });
      }
      catch (Exception e)
      {
        var message = $@"General failure: {e.Message}

Please submit an issue at: https://gitlab.com/under-test/undertest.featuretransform/-/issues/new?

Include the message: {e.Message}

Stack trace: {e.StackTrace}";

        Console.WriteLine(message);
        return (int)ExitCodes.GeneralFailure;
      }

      return (int)ExitCodes.Success;
    }
  }
}
