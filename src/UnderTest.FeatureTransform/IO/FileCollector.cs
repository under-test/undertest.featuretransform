using System.Collections.Generic;
using System.IO;
using System.Linq;
using GlobExpressions;

namespace UnderTest.FeatureTransform.IO
{
  public static class FileCollector
  {
    public static IEnumerable<string> CollectFilesFromGlobs(string directoryP, IEnumerable<string> globsP)
    {
      var result = new List<string>();

      foreach (var file_path in globsP)
      {
        // if the file exists - add it
        // if this is a directory - find all feature files in the directory
        // otherwise we will glob for the files within the working directory
        if (File.Exists(file_path))
        {
          result.Add(file_path);
          continue;
        }

        if (Directory.Exists(file_path))
        {
          result.AddRange(Glob.Files(file_path, "**/*.feature").ToList());
          continue;
        }

        result.AddRange(Glob.Files(directoryP, file_path));
      }

      return result;
    }
  }
}
