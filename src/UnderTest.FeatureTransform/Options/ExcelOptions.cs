using System.Collections.Generic;
using CommandLine;

namespace UnderTest.FeatureTransform.Options
{
  [Verb("excel", HelpText = "Create excel workbook from feature files")]
  public class ExcelOptions : IStandardVerbOptions 
  {
    [Value(0, MetaName = "filepaths", Required = false, Default = new[] { "**/*.feature" }, HelpText = "Input filepath(s).  Supports globs, absolute and relative paths.")]
    public IEnumerable<string> FeaturePaths { get; set; }

    [Option('f', "filename", Default = "TestFeatureSheets.xlsx", Required = false, HelpText = "Output filename.")]
    public string OutputFilename { get; set; }

    [Option('w', "workingDirectory", Default = null, Required = false, HelpText = "Folder to execute this command in.  Defaults to the current directory.")]
    public string WorkingDirectory { get; set; }
  }
}
