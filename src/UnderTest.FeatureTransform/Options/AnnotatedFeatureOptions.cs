﻿using System.Collections.Generic;
using CommandLine;

namespace UnderTest.FeatureTransform.Options
{
  [Verb("annotate", HelpText = "Create annotated markdown versions of feature files.")]
  public class AnnotatedFeatureOptions: IStandardVerbOptions
  {
    [Value(0, MetaName = "filepaths", Required = false, Default = new[] {"**/*.feature"},
      HelpText = "Input filepath(s).  Supports globs, absolute and relative paths.")]
    public IEnumerable<string> FeaturePaths { get; set; }

    [Option('f', "filename", Default = "AnnotatedFeature.md", Required = false, HelpText = "Output filename.")]
    public string OutputFilename { get; set; }

    [Option('w', "workingDirectory", Default = null, Required = false,
      HelpText = "Folder in which to execute this this command.  Defaults to the current directory.")]
    public string WorkingDirectory { get; set; }
  }
}
