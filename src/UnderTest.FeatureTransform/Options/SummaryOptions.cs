using CommandLine;

namespace UnderTest.FeatureTransform.Options
{
  using System.Collections.Generic;

  [Verb("summary", HelpText = "Create markdown summary page of features in project")]
  public class SummaryOptions : IStandardVerbOptions
  {
    [Value(0, MetaName = "filepaths", Required = false, Default = new[] { "**/*.feature" }, HelpText = "Input filepath(s).  Supports globs, absolute and relative paths.")]
    public IEnumerable<string> FeaturePaths { get; set; }

    [Option('f', "filename", Default = "FeatureSummary.md", Required = false, HelpText = "Output filename.")]
    public string OutputFilename { get; set; }

    [Option('w', "workingDirectory", Default = null, Required = false, HelpText = "Folder to execute this command in.  Defaults to the current directory.")]
    public string WorkingDirectory { get; set; }
  }
}
