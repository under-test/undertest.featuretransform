﻿using System;
using System.Text;
using Gherkin.Ast;

namespace UnderTest.FeatureTransform.Summary
{
  public class FeatureSummaryDetails
  {
    public string FeatureName { get; private set; }
    public string Description { get; private set; }
    public string ScenarioList { get; private set; }

    public void SetDetails(GherkinDocument featureDocumentP)
    {
      FeatureName = featureDocumentP.Feature.Name;
      Description = featureDocumentP.Feature.Description;
      ScenarioList = GenerateScenarioList(featureDocumentP);
      if (!string.IsNullOrWhiteSpace(Description))
      {
        Description = featureDocumentP.Feature.Description
          .Replace(Environment.NewLine, "<br />");
      }
      if (string.IsNullOrWhiteSpace(FeatureName))
      {
        FeatureName = "(empty feature name)";
      }
      if (string.IsNullOrWhiteSpace(Description))
      {
        Description = "`no description`";
      }
    }

    public void SetInvalidDetails()
    {
      FeatureName = "Invalid file";
      Description = string.Empty;
      ScenarioList = string.Empty;
    }

    private static string GenerateScenarioList(GherkinDocument gherkinDocumentP)
    {
      var listOfScenarios = new StringBuilder();
      listOfScenarios.Append("<details><summary>Scenarios</summary><ul>");
      foreach (var child in gherkinDocumentP.Feature.Children)
      {
        if (child is Scenario scenario)
        {
          listOfScenarios.Append($"<li>{scenario.Name}</li>");
        }
      }

      listOfScenarios.Append("</ul></details>");
      return listOfScenarios.ToString();
    }
  }
}
