using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnderTest.FeatureTransform.Options;
using static UnderTest.FeatureTransform.Constants.FeatureSummaryConstants;

namespace UnderTest.FeatureTransform.Summary
{
  public class FeatureSummary
  {
    public FeatureSummary(IStandardVerbOptions options, IEnumerable<string> files)
    {
      _options = options;
      _files = files;
    }

    private readonly StringBuilder _summaryContent = new StringBuilder();
    private string _rootDirName;
    private readonly IStandardVerbOptions _options;
    private readonly IEnumerable<string> _files;
    private void AddTocDirectoryList(IEnumerable<string> directories)
    {
      foreach (var directory in directories)
      {
        _summaryContent.AppendLine(directory == ""
          ? $"* [{_rootDirName}](#{_rootDirName.MakeHeaderLink()})"
          : $"* [{directory}](#{directory.MakeHeaderLink()})");
      }
    }

    private void AddDirectoryTitleAndTableHeaders(string directoryP)
    {
      string displayDirectoryName;
      if (directoryP == "")
      {
        displayDirectoryName = _rootDirName;
      }
      else
      {
        displayDirectoryName = directoryP.Replace(_options.WorkingDirectory, string.Empty) + "\\";
      }
      _summaryContent.AppendLine($"## {displayDirectoryName}");
      _summaryContent.AppendLine(SummaryHeader);
      _summaryContent.AppendLine(SummaryDivider);
    }

    private void AddFeaturesToDirectoryTable(IEnumerable<string> directoryFeaturesP)
    {
      foreach (var feature in directoryFeaturesP)
      {
        AddSingleFeatureToDirectoryTable(feature);
      }
    }

    private void AddSingleFeatureToDirectoryTable(string feature)
    {
      var summaryDetails = new FeatureSummaryDetails();
      try
      {
        var gherkinDocument = new Gherkin.Parser().Parse(Path.Combine(_options.WorkingDirectory, feature));
        summaryDetails.SetDetails(gherkinDocument);
      }
      catch
      {
        summaryDetails.SetInvalidDetails();
      }

      _summaryContent.AppendLine($"| [{summaryDetails.FeatureName}](.\\{Uri.EscapeUriString(feature).UseBackslashForDirectories()}) | {summaryDetails.Description} | {summaryDetails.ScenarioList} |");
    }

    private void AddEachDirectoryFeatureSummaryTable(IEnumerable<string> directoriesP)
    {
      foreach (var directory in directoriesP)
      {
        AddDirectoryTitleAndTableHeaders(directory);

        var directoryFeatures = _files.Where(x => Path.GetDirectoryName(x) == directory).ToList();
        AddFeaturesToDirectoryTable(directoryFeatures);

        _summaryContent.AppendLine();
      }
    }

    public void CreateFeatureSummary()
    {
      _rootDirName = new DirectoryInfo(_options.WorkingDirectory).Name;

      var featureDirectories = _files.Where(x => !string.IsNullOrEmpty(x))
                                    .Select(Path.GetDirectoryName)
                                    .Distinct();

      _summaryContent.AppendLine(Title);
      _summaryContent.AppendLine(TocTitle);

      var directoriesP = featureDirectories as string[] ?? featureDirectories.ToArray();
      AddTocDirectoryList(directoriesP);
      AddEachDirectoryFeatureSummaryTable(directoriesP);

      File.WriteAllText(Path.Combine(_options.WorkingDirectory, _options.OutputFilename), _summaryContent.ToString());
    }
  }
}
