﻿namespace UnderTest.FeatureTransform.Constants
{
  public static class FeatureReportConstants
  {
    public const string RunTableHeader = "| Start Time | End Time | Duration |\r\n |---|---|---|";
    public const string TestResultTableHeader = "|Keyword|Step|Result|Arguments|Messages|Duration|\r\n |---|---|---|---|---|---|---|";
  }
}
