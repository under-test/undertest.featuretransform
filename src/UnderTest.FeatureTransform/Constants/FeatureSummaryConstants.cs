﻿namespace UnderTest.FeatureTransform.Constants
{
  public static class FeatureSummaryConstants
  {
    public const string Title = "# Feature Summary";
    public const string TocTitle = "## TOC";
    public const string SummaryHeader = "| Feature | Description | Scenarios |";
    public const string SummaryDivider = "| --- | --- | --- |";
  }
}
