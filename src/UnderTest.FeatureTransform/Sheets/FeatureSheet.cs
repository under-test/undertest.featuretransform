using System.Collections.Generic;
using System.Linq;
using ClosedXML.Excel;
using Gherkin.Ast;
using Scenario = Gherkin.Ast.Scenario;

namespace UnderTest.FeatureTransform.Sheets
{
  public class FeatureSheet
  {
    public FeatureSheet(XLWorkbook workBookP, GherkinDocument featureDocP, List<string> resultOptionsP)
    {
      _workBook = workBookP;
      _name = featureDocP.Feature.Name.MakeNameWorkSheetAcceptable();
      _feature = featureDocP;
      _resultOptions = resultOptionsP;
    }

    private readonly XLWorkbook _workBook;
    private readonly string _name;
    private readonly GherkinDocument _feature;
    private readonly List<string> _resultOptions;
    public readonly List<IXLCell> FeatureResultCells = new List<IXLCell>();

    private const int StartingRow = 3;
    private const int StartingColumn = 3;

    private int _currentCellRowIndex = StartingRow;
    private int _currentCellColIndex = StartingColumn;

    public FeatureSheet Build()
    {
      CreateFeatureSheet();

      return this;
    }

    private void CreateResultCell(IXLWorksheet workSheetP, int row)
    {
      var resultCell = workSheetP.Cell($"A{row}");

      resultCell.SetDataValidation().List(_workBook.Worksheet("Result Options").Range($"A1:A{_resultOptions.Count+1}"));
      resultCell.Style.Fill.BackgroundColor = XLColor.LightGray;
      resultCell.AddConditionalFormat().WhenEquals("Pass").Fill.BackgroundColor = XLColor.Green;
      resultCell.AddConditionalFormat().WhenEquals("Fail").Fill.BackgroundColor = XLColor.Red;
      resultCell.AddConditionalFormat().WhenEquals("Incomplete").Fill.BackgroundColor = XLColor.Yellow;
      FeatureResultCells.Add(resultCell);
    }

    private void AddDocString(IXLWorksheet workSheetP, DocString docStringP)
    {
      workSheetP.Cell(_currentCellRowIndex, _currentCellColIndex).Value =
        docStringP.Content;
      _currentCellRowIndex++;
    }

    private void AddExampleHeader(IXLWorksheet workSheetP, TableCell headerP)
    {
      workSheetP.Cell(_currentCellRowIndex, _currentCellColIndex).Value = headerP.Value;
      workSheetP.Cell(_currentCellRowIndex, _currentCellColIndex).Style.Fill.BackgroundColor = XLColor.LightGray;
      _currentCellColIndex++;
    }

    private void AddExampleTableRow(IXLWorksheet workSheetP, TableRow rowP)
    {
      CreateResultCell(workSheetP, _currentCellRowIndex);
      foreach (var cell in rowP.Cells)
      {
        workSheetP.Cell(_currentCellRowIndex, _currentCellColIndex).Value = cell.Value;
        _currentCellColIndex++;
      }

      _currentCellRowIndex++;
      _currentCellColIndex = 4;
    }

    private void AddExampleTable(IXLWorksheet workSheetP, IEnumerable<Examples> examplesP)
    {
      foreach (var example in examplesP)
      {
        workSheetP.Cell(_currentCellRowIndex, _currentCellColIndex).Value = example.Keyword;
        workSheetP.Cell(_currentCellRowIndex, _currentCellColIndex).Style.Font.Bold = true;
        CreateResultCell(workSheetP, _currentCellRowIndex);
        _currentCellRowIndex++;
        _currentCellColIndex++;
        foreach (var header in example.TableHeader.Cells)
        {
          AddExampleHeader(workSheetP, header);
        }

        _currentCellColIndex = 4;
        _currentCellRowIndex++;
        foreach (var row in example.TableBody)
        {
          AddExampleTableRow(workSheetP, row);
        }

        _currentCellColIndex = StartingColumn;
      }
    }

    private void AddStep(IXLWorksheet workSheetP, Step stepP)
    {
      workSheetP.Cell(_currentCellRowIndex, _currentCellColIndex).Value = stepP.Keyword;
      workSheetP.Cell(_currentCellRowIndex, _currentCellColIndex).Style.Font.Bold = true;
      CreateResultCell(workSheetP, _currentCellRowIndex);
      _currentCellColIndex++;
      workSheetP.Cell(_currentCellRowIndex, _currentCellColIndex).Value = stepP.Text;
      _currentCellRowIndex++;
      _currentCellColIndex--;
    }

    private void AddFeatureSheetHeaders(IXLWorksheet workSheetP)
    {
      workSheetP.Cell("A1").Value = _feature.Feature.Keyword;
      workSheetP.Cell("A1").Style.Font.Bold = true;
      workSheetP.Cell("B1").Value = _feature.Feature.Name;
      workSheetP.Cell("B1").Style.Font.Bold = true;
      workSheetP.Cell("A2").Value = "Results";
    }

    private void AddScenarioKeywordAndName(IXLWorksheet workSheetP, IHasDescription scenarioP)
    {
      workSheetP.Cell(_currentCellRowIndex, 2).Value = scenarioP.Keyword;
      workSheetP.Cell(_currentCellRowIndex, 2).Style.Font.Bold = true;
      workSheetP.Cell(_currentCellRowIndex, 3).Value = scenarioP.Name;
      workSheetP.Cell(_currentCellRowIndex, 3).Style.Font.Italic = true;
      CreateResultCell(workSheetP, _currentCellRowIndex);
      _currentCellRowIndex++;
    }

    private void AddDataTable(IXLWorksheet workSheetP, IHasRows dataTableP)
    {
      for (var row = 0; row < dataTableP.Rows.Count(); row++)
      {
        for (var col = 0; col < ((TableRow[])dataTableP.Rows)[0].Cells.Count(); col++)
        {
          workSheetP.Cell(_currentCellRowIndex, _currentCellColIndex).Value = ((TableCell[])((TableRow[])dataTableP.Rows)[row].Cells)[col].Value;
          if (row == 0)
          {
            workSheetP.Cell(_currentCellRowIndex, _currentCellColIndex).Style.Fill.BackgroundColor = XLColor.LightGray;
          }
          _currentCellColIndex++;
        }

        _currentCellRowIndex++;
        _currentCellColIndex = 4;
      }
    }

    private void AddStepKeywordAndText(IXLWorksheet workSheetP, Step stepP)
    {
      workSheetP.Cell(_currentCellRowIndex, _currentCellColIndex).Value = stepP.Keyword;
      workSheetP.Cell(_currentCellRowIndex, _currentCellColIndex).Style.Font.Bold = true;
      CreateResultCell(workSheetP, _currentCellRowIndex);
      _currentCellColIndex++;
      workSheetP.Cell(_currentCellRowIndex, _currentCellColIndex).Value = stepP.Text;

      _currentCellRowIndex++;
    }

    private void AddScenarioSteps(IXLWorksheet workSheetP, IEnumerable<Step> stepsP)
    {
      foreach (var step in stepsP)
      {
        if (step.Argument != null)
        {
          AddStepKeywordAndText(workSheetP, step);

          if (step.Argument.GetType() == typeof(DataTable))
          {
            AddDataTable(workSheetP, (DataTable)step.Argument);
          }

          if (step.Argument.GetType() == typeof(DocString))
          {
            AddDocString(workSheetP, (DocString)step.Argument);
          }

          _currentCellColIndex = StartingColumn;
        }
        else
        {
          AddStep(workSheetP, step);
        }
      }
    }

    private void AddScenarios(IXLWorksheet workSheetP, IList<IHasLocation> children)
    {
      foreach (var child in children)
      {
        if (child is Rule rule)
        {
          AddRuleHeader(workSheetP, rule);
          AddScenarios(workSheetP, rule.Children.ToList());
        }

        if (!(child is Scenario scenario))
        {
          continue;
        }

        AddScenario(workSheetP, scenario);
      }
    }

    private void AddRuleHeader(IXLWorksheet workSheetP, Rule rule)
    {
      workSheetP.Cell(_currentCellRowIndex, 2).Value = rule.Keyword;
      workSheetP.Cell(_currentCellRowIndex, 2).Style.Font.Bold = true;
      workSheetP.Cell(_currentCellRowIndex, 3).Value = rule.Name;
      workSheetP.Cell(_currentCellRowIndex, 3).Style.Font.Italic = true;
      _currentCellRowIndex++;

      if (!string.IsNullOrWhiteSpace(rule.Description))
      {
        workSheetP.Cell(_currentCellRowIndex, 3).Value = rule.Description;
        workSheetP.Cell(_currentCellRowIndex, 3).Style.Font.Italic = true;
        _currentCellRowIndex++;
      }
    }

    private void AddScenario(IXLWorksheet workSheetP, Scenario scenario)
    {
      AddScenarioKeywordAndName(workSheetP, scenario);
      AddScenarioSteps(workSheetP, scenario.Steps);
      CheckForScenarioOutlineAndPrintExampleTable(scenario, workSheetP);

      _currentCellRowIndex++;
    }

    private void CheckForScenarioOutlineAndPrintExampleTable(Scenario scenarioP, IXLWorksheet workSheetP)
    {
      if (!(scenarioP is Scenario scenarioOutline) || !scenarioOutline.Examples.Any()) return;
      _currentCellRowIndex++;
      AddExampleTable(workSheetP, scenarioOutline.Examples);
    }

    private void CreateFeatureSheet()
    {
      var workSheet = _workBook.Worksheets.Add(_name);
      AddFeatureSheetHeaders(workSheet);
      AddScenarios(workSheet, _feature.Feature.Children.ToList());
    }
  }
}
