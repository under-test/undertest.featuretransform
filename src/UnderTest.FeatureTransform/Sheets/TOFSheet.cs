using ClosedXML.Excel;
using System.Collections.Generic;
using System.Linq;

namespace UnderTest.FeatureTransform.Sheets
{
  public class TofSheet
  {
    private readonly XLWorkbook _workbook;
    private const string Name = "Table of Features";

    public TofSheet(XLWorkbook wbP)
    {
      _workbook = wbP;
      CreateTableOfFeaturesSheet();
    }

    public void CreateFeatureEntry(string featureNameP, IEnumerable<IXLCell> featureResultCellsP)
    {
      string BuildFeatureCountRollupForText(IXLCell[] resultCellsP1, string sheetName, string filterText)
      {
        var cells = resultCellsP1
          .Select(
            a => $"ISNUMBER(SEARCH(\"{filterText}\",'{sheetName}'!A{a.Address.RowNumber}))")
          .ToList();

        if (!cells.Any())
        {
          return "0";
        }

        return cells
          .Aggregate(
            (a, b) => $"{a},{b}");
      }

      string BuildFailureFormula(IXLCell[] resultCellsP1, string theFeatureSheetName)
      {
        const string failText = "Fail";
        return BuildFeatureCountRollupForText(resultCellsP1, theFeatureSheetName, failText);
      }

      string BuildPassFormula(IXLCell[] xlCells, string theFeatureSheetName)
      {
        const string passText = "Pass";
        return BuildFeatureCountRollupForText(xlCells, theFeatureSheetName, passText);
      }

      var featureSheetName = featureNameP.MakeNameWorkSheetAcceptable();
      var resultCellsP = featureResultCellsP as IXLCell[] ?? featureResultCellsP.ToArray();
      var checkPassFormula = BuildPassFormula(resultCellsP, featureSheetName);
      var checkFailFormula = BuildFailureFormula(resultCellsP, featureSheetName);
      var currentRow = 1;
      var tofSheet = _workbook.Worksheet(Name);
      var lastRowUsed = tofSheet.LastRowUsed();
      if (lastRowUsed != null)
      {
        currentRow = tofSheet.LastRowUsed().RowNumber() + 1;
      }

      const int currentCol = 1;
      var summaryCell = tofSheet.Cell(currentRow, currentCol + 1);
      tofSheet.Cell(currentRow, currentCol).Value = featureNameP;
      tofSheet.Cell(currentRow, currentCol).Hyperlink = new XLHyperlink(featureSheetName + "!A1");
      summaryCell.FormulaA1 =
        $"=IF(AND({checkPassFormula}),\"Pass\",IF(OR({checkFailFormula}),\"Fail\",\"Incomplete\"))";
      summaryCell.Style.Fill.BackgroundColor = XLColor.LightGray;
      summaryCell.AddConditionalFormat().WhenEquals("Pass").Fill.BackgroundColor = XLColor.Green;
      summaryCell.AddConditionalFormat().WhenEquals("Fail").Fill.BackgroundColor = XLColor.Red;
      summaryCell.AddConditionalFormat().WhenEquals("Incomplete").Fill.BackgroundColor = XLColor.Yellow;
    }

    private void CreateTableOfFeaturesSheet()
    {
      _workbook.Worksheets.Add(Name);
    }


    public void SetPosition(int positionP)
    {
      _workbook.Worksheets.Worksheet(Name).Position = positionP;
    }

    public void CreateInvalidFeatureEntry(string invalidFeatureNameP)
    {
      var newRow = 1;
      var tofSheet = _workbook.Worksheet(Name);
      var lastRowUsed = tofSheet.LastRowUsed();
      if (lastRowUsed != null)
      {
        newRow = tofSheet.LastRowUsed().RowNumber() + 1;
      }

      const int currentCol = 1;

      tofSheet.Cell(newRow, currentCol).Value = $"Invalid Feature: {invalidFeatureNameP}";
      tofSheet.Cell(newRow, currentCol).Style.Font.FontColor = XLColor.Red;
    }
  }
}
