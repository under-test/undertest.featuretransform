Feature: Refund Purchase

CXs need to return purchased items.

Scenario: CX return item
  Given a customer bought a TV
  When the customer returns the TV
  Then the customer gets their money back