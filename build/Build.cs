using System;
using Nuke.Common;
using Nuke.Common.Git;
using Nuke.Common.ProjectModel;
using Nuke.Common.Tooling;
using Nuke.Common.Tools.DotNet;
using Nuke.Common.Tools.Git;
using Nuke.Common.Tools.GitVersion;
using Nuke.Common.Tools.NuGet;
using Nuke.Common.Tools.NUnit;
using Nuke.Common.Utilities;
using Nuke.Common.Utilities.Collections;

using static Nuke.Common.IO.FileSystemTasks;
using static Nuke.Common.IO.PathConstruction;
using static Nuke.Common.Tools.Git.GitTasks;
using static Nuke.Common.Tools.DotNet.DotNetTasks;
using static Nuke.Common.Tools.NUnit.NUnitTasks;
using static Nuke.Common.Tools.NuGet.NuGetTasks;
using static Tools.NugetPushAdditions;

class Build : NukeBuild
{
  public static int Main() => Execute<Build>(x => x.Pack);

  [Parameter("Configuration to build - Default is 'Debug' (local) or 'Release' (server)")]
  readonly string Configuration = IsLocalBuild ? "Debug" : "Release";

  [Solution("src/UnderTest.FeatureTransform.sln")]
  readonly Solution Solution;

  Project CommandLintProject => Solution.GetProject("UnderTest.FeatureTransform").NotNull();

  [GitRepository]
  readonly GitRepository GitRepository;

  [GitVersion]
  readonly GitVersion GitVersion;

  static AbsolutePath SourceDirectory => RootDirectory / "src";

  AbsolutePath CliProjectPath = SourceDirectory / "UnderTest.FeatureTransform" / "UnderTest.FeatureTransform.csproj";

  AbsolutePath AcceptanceTestProjectDirectory = SourceDirectory / "UnderTest.FeatureTransform.AcceptanceTests" / "UnderTest.FeatureTransform.AcceptanceTests.csproj";

  AbsolutePath ArtifactsDirectory => RootDirectory / "artifacts";

  [Parameter("Api Key for Nuget.org when pushing our nuget package")]
  readonly string NugetOrgApiKey;

  const string TargetNugetServer = "https://www.nuget.org/api/v2/package";

  Target Clean => _ => _
    .Executes(() =>
    {
      foreach (var directory in GlobDirectories(SourceDirectory, "**/bin", "**/obj"))
      {
        DeleteDirectory(directory);
      }
      EnsureCleanDirectory(ArtifactsDirectory);
    });

  Target Restore => _ => _
    .DependsOn(Clean)
    .Executes(() =>
    {
      DotNetRestore(s => s
        .SetProjectFile(Solution));
    });

  Target Compile => _ => _
    .DependsOn(Restore)
    .Executes(() =>
    {
      DotNetBuild(s => s
        .SetProjectFile(Solution)
        .SetConfiguration(Configuration)
        .EnableNoRestore());

      DotNetPublish(s => s
        .EnableNoRestore()
        .SetConfiguration(Configuration)
        .SetProject(CommandLintProject));
    });

  public Target UnitTests =>
    _ =>
      _.DependsOn(Compile)
        .Executes(() =>
        {
          Solution.GetProjects("*.UnitTests")
            .ForEach(x => DotNetTest(s => s
              .SetProjectFile(x)
              .SetConfiguration(Configuration)
              .EnableNoBuild()
              .SetLogger("trx")
              .SetResultsDirectory(ArtifactsDirectory)));
        });

  public Target AcceptanceTests =>
    _ =>
      _.DependsOn(Compile)
        .Executes(() =>
        {
          Solution.GetProjects("*.AcceptanceTests")
            .ForEach(x =>
              NUnit3(s => s
                .AddInputFiles(GlobFiles(AcceptanceTestProjectDirectory,
                    $"**/bin/{Configuration}/*.AcceptanceTests.dll")
                  .NotEmpty())
                .SetToolPath(ToolPathResolver.GetPackageExecutable("NUnit.ConsoleRunner", "nunit3-console.exe"))));
        });

  Target Pack => _ => _
    .DependsOn(AcceptanceTests)
    .DependsOn(UnitTests)
    .Executes(() =>
    {
      DotNetPack(s => s
        .SetProject(CliProjectPath)
        .EnableNoBuild()
        .SetConfiguration(Configuration)
        .EnableIncludeSymbols()
        .SetSymbolPackageFormat(DotNetSymbolPackageFormat.snupkg)
        .SetOutputDirectory(ArtifactsDirectory)
        .SetVersion(GitVersion.NuGetVersionV2));
    });

  public Target Push =>
    _ =>
      _.DependsOn(Pack)
        .OnlyWhenDynamic(() => OnABranchWeWantToPushToNugetOrg())
        .Requires(() => NugetOrgApiKey)
        .Requires(() => GitHasCleanWorkingCopy())
        .Requires(() => IsReleaseConfiguration())
        .Executes(() =>
        {
          GlobFiles(ArtifactsDirectory, "*.nupkg")
            .ForEach(x =>
            {
              NugetPushIfVersionDoesNotExistOnNugetServer(
                x,
                y => NuGetPush(s => s
                  .SetTargetPath(x)
                  .SetSource(TargetNugetServer)
                  .SetApiKey(NugetOrgApiKey)));
              });
            });

  public bool IsReleaseConfiguration()
  {
    return Configuration.EqualsOrdinalIgnoreCase("Release");
  }

  // our branching strategy is stable for production, release* for releases and hotfix* for hotfix items.
  public bool OnABranchWeWantToPushToNugetOrg()
  {
    var branch = this.GetCurrentBranch().ToLowerInvariant();
    Logger.Log(LogLevel.Normal, $"Current branch {branch}");

    return branch == "stable"
           || branch.StartsWith("release")
           || branch.StartsWith("hotfix");
  }

  private string GetCurrentBranch()
  {
    var environmentVariable = Environment.GetEnvironmentVariable("APPVEYOR_REPO_BRANCH");
    if (!string.IsNullOrEmpty(environmentVariable))
    {
      return environmentVariable;
    }

    return GitCurrentBranch();
  }
}
